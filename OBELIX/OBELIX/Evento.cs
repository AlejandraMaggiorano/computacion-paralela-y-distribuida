﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OBELIX
{
    public class Evento
    {
        private readonly string tipoEvento;
        List<int> puntajes = new List<int>(); 
    

        Evento( string tipo)
        {
            tipoEvento = tipo;

        }//Evento constructor

        public string getTipoEvento()
        {
            return tipoEvento;
        }//getTipoEvento

      public void AgregarPuntaje(int puntaje)
        {
            puntajes.Add(puntaje);
        }

        public string getUlitmoPuntaje()
        {
            if (puntajes.Count != 0)
            {
                return puntajes.Last().ToString();
            }

            else return "No disponbible";

        }//GetUltimoPuntaje
    }
}