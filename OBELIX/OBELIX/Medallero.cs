﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OBELIX
{
    public class Medallero
    {
        private int cantMedallasOro;
        private int cantMedallasPlata;
        private int cantMedallasBronce;

        Medallero()
        {
            //Constructor por defecto
            cantMedallasBronce = 0;
            cantMedallasOro = 0;
            cantMedallasPlata = 0;
        }

        public int getCantMedallasOro()
        {
            return cantMedallasOro;
        }//getCantMedallasOro

        public int getCantMedallasPlata()
        {
            return cantMedallasPlata; 
                }//getCantMedallasPlata

        public int getCantMedallasBronce()
        {
            return cantMedallasBronce;
        }//getCantMedallasBronce

        public void addMedallaOro()
        {
            cantMedallasOro++;
        }//addMedallaOro

        public void addMedallaPlata()
        {
            cantMedallasPlata++;
        }//addMedallaPlata

        public void addMedallaBronce()
        {
            cantMedallasBronce++;
        }//addMeedallaBronce

    }//Class Medallero
}