﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace OBELIX
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://galosencuarentena.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
       
        public Olimpiada olimpiadaactual = new Olimpiada();

        private WebService1() 
            {
           
            }


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string GetMedallero(string equipo )
        {
            //Faltan los input
            return olimpiadaactual.getMedallero(equipo);
        }

        [WebMethod]
        public string SetScore(string tipoEvento, int puntaje)
        {
            foreach( Evento ev in olimpiadaactual.EventosActuales)
            {
                if (ev.getTipoEvento().ToUpper() == tipoEvento.ToUpper())
                {
                    ev.AgregarPuntaje(puntaje);
                    return "Puntaje actualizado exitosamente";
                }
            }
            return "Puntaje no actualizado, evento no hallado.";
        }

        [WebMethod]
        public string GetScore(string tipoEvento)
        {
            foreach (Evento ev in olimpiadaactual.EventosActuales)
            {
                if (ev.getTipoEvento().ToUpper() == tipoEvento.ToUpper())
                {
                    return "Último puntaje: " + ev.getUlitmoPuntaje();
                }
            }
            return "Puntaje no accesible, evento no hallado.";
        }

        [WebMethod]
        public string RegisterClient(string clientID, string tipoEvento)
        {
            return "Hello World";
        }


    }
}
