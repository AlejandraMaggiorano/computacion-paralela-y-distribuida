﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OBELIX
{
    public class Olimpiada
    {
        private int year;
        private bool temporada; // true es verano, false es invierno
        public List<Evento> EventosActuales = new List<Evento>();
        public List<Equipo> EquiposActuales = new List<Equipo>();
        
        public Olimpiada ()
        {
            //Relleno con valores default en este constructor
            year = 2021;
            temporada = true;
        }

        public string getMedallero(string equipo)
        {
            //Devuelve el medallero del equipo seleccionado

            string salida = "No hay equipos disponibles con ese nombre.";
            foreach(Equipo e in EquiposActuales)
            {
                if (e.nombre.ToUpper() == equipo.ToUpper())
                {
                    salida = "Oro: " + e.Medallas.getCantMedallasOro().ToString() + " Plata: " + e.Medallas.getCantMedallasPlata().ToString() + " Bronce: " + e.Medallas.getCantMedallasBronce();
                }
            }

            return salida;
        }//getMedallero

        public string getScore(string tipoEvento)
        {
            string puntaje = "Puntajes no disponibles para el evento";
            foreach (Evento e in EventosActuales)
            {
                if(tipoEvento.ToUpper()==e.getTipoEvento())
                {
                    return e.getUlitmoPuntaje();
                }
            }

            return puntaje;
        }


    }
}