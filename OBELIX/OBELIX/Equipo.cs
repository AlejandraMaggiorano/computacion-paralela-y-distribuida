﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OBELIX
{
    public class Equipo
    {
        public readonly string nombre;
        public Medallero Medallas;
        public List<Evento> Eventos = new List<Evento>();

        Equipo(string nom)
        {
            nombre = nom;

        }//Equipo constructor

        public string getNombre()
        {
            return nombre;
        }//getNombre

        public void incrementMedallero(string tipoEvento, string tipoMedalla)
        {
            foreach (Evento ev in Eventos)
            {
                if (ev.getTipoEvento().ToUpper()== tipoEvento.ToUpper())
                {
                    if(tipoMedalla.ToUpper()== "ORO")
                    {
                        Medallas.addMedallaOro();
                    }
                    if(tipoMedalla.ToUpper() == "PLATA")
                    {
                        Medallas.addMedallaPlata();
                    }
                    if(tipoMedalla.ToUpper()== "BRONCE")
                    {
                        Medallas.addMedallaBronce();
                    }
                }
            }
        }



    }//clase equipo
}