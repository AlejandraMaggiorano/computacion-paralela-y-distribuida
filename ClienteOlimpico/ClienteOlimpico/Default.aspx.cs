﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BotonEquipo_Click(object sender, EventArgs e)
    {
        ServiceReference1.WebService1SoapClient client = new ServiceReference1.WebService1SoapClient();
        lblMedallero.Text = client.GetMedallero(txtEquipo.Text);
    }

 

    protected void BotonEvento_Click(object sender, EventArgs e)
    {
        ServiceReference1.WebService1SoapClient client = new ServiceReference1.WebService1SoapClient();
        lblPuntaje.Text = client.GetScore(txtEvento.Text);
    }
}