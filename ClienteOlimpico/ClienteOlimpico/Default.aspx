﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Portal Olímpico</title>
</head>
<body>
    <img src ="banner.png" />
    <form id="form1" runat="server">
        <div>
            <table style="font-family:Verdana">
                <tr>
                    <td>
                        Ingrese equipo: <asp:TextBox ID="txtEquipo" runat="server" ></asp:TextBox>
                        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMedallero" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="BotonEquipo" runat="server" Text="Seleccionar" OnClick="BotonEquipo_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Ingrese evento: <asp:TextBox ID="txtEvento" runat="server" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPuntaje" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Button ID="BotonEvento" runat="server" Text="Seleccionar" OnClick="BotonEvento_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>

    <script src="~/Scripts000000></script>
</body>
</html>
